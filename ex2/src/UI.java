import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class UI extends JFrame {


    JLabel L1, L2;
    JButton B1;
    JTextField T1,T2;

    UI() {
        setTitle("Baza de date");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1280, 720);
        setLocation(850, 250);
        init();
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        L1 = new JLabel("Text1");
        L1.setBounds(10, 50, 150, 20);
        T1 = new JTextField();
        T1.setBounds(150, 50, 150, 20);

        L2 = new JLabel("Text2");
        L2.setBounds(10, 100, 150, 20);
        T2 = new JTextField();
        T2.setBounds(150, 100, 150, 20);


        B1 = new JButton("Add");
        B1.setBounds(10, 350, 150, 20);
        B1.addActionListener(new BTN1action());

        add(L1);
        add(T1);
        add(L2);
        add(T2);
        add(B1);

    }
    public static void main(String[] args) {
        new UI();
    }

    class BTN1action implements ActionListener {
     @Override
        public void actionPerformed(ActionEvent e) {
            String fisier=T1.getText();
            String text=T2.getText();
            FileWriter f = null;
            try {
                 f = new FileWriter(fisier);
                 f.write(text);
             }
             catch (IOException ioException) {
                 System.out.println("Nu exista fisier");
             }
         }
        }

        }

